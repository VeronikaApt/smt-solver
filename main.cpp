#include "task.h"

int main(int argc, char *argv[]) {

	if (argc < 2) {
		cerr << "Error 1: You should put the name of the file\n";
		exit(1);
	}
	
	ifstream input;
	input.open(argv[1], ios::in);
	if (input.fail()) {
		cerr << "Error 2: Cannot open file: " << argv[1] << "\n";
		exit(2);
	}

	Task task;

	task.readData(input);

	task.solve();
	
	return 0;
}