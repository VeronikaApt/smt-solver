#include "simplex_method.h"

string SimplexMethod::defineBasis(vector<string> &equations) {
	vector<string>::iterator it;
	int length;

	for (it = equations.begin(); it != equations.end(); ++it) {
		length = it->length();
		for (int i = 1; i < length; i++) {
			if (((*it)[i] == '>') || (((*it)[i-1] != '<') && ((*it)[i] == '=')))
				return "artificial";
		}													
	}											

	return "natural";
}

int SimplexMethod::literalInd(char ch, char *column_literals) {
	int size = 0;
	while (column_literals[size] != '\0') 
		size++;

	for (int i = 0; i < size; i++) 
		if (column_literals[i] == ch)
			return i;

	return -1; 
}

bool SimplexMethod::isElementBelowZero(double **table, char *column_literals) {
	double d;
	int size = 0;
	while (column_literals[size] != '\0') 
		size++;

	for (int i = 0; i < size; i++) {
		d = table[0][i];
		if (table[0][i] < 0)
			return true;
	}

	return false;
}

bool SimplexMethod::allElementBelowZero(double **table, char *row_literals, int j) {
	int size = 0;
	while (row_literals[size] != '\0') 
		size++;

	for (int i = 0; i < size; i++)
		if (table[i][j] > 0)
			return false;

	return true;
}

void SimplexMethod::choosePivotColumn(double **table, int &j, char *column_literals) {
	double min;
	int size = 0, ind = 1;
	while (column_literals[size] != '\0') 
		size++;

	/*cout << endl << endl << "************" << endl;

	for (int i = 0; i < 3; i++) {
		for (int z = 0; z < 7; z++)
			cout << table[i][z] << " ";
		cout << endl;
	}*/

	min = table[0][1];
	for (int i = 2; i < size; i++)
		if (table[0][i] < min) {
			min = table[0][i];
			ind = i;
		}

	j = ind;
}

//void SimplexMethod::choosePivotColumn(int &j, double *estimation, int size) {
//	int min = estimation[0];
//	j = 0;
//
//	for (int i = 1; i < size; i++) {
//		if (estimation[i] < min) {
//			min = estimation[i];
//			j = i;
//		}
//	}
//
//	j++;
//}

void  SimplexMethod::choosePivotRow(double **table, int &i, int j, char *row_literals) {
	int size = 0, ind = 1, count = 0;
	while (row_literals[size] != '\0') 
		size++;
	double min = 0;
	double *estimate = new double[size - 1];
	int *est_ind = new int[size - 1];

	/*cout << endl << endl << "---------------" << endl;

	for (i = 0; i < 3; i++) {
		for (int z = 0; z < 7; z++)
			cout << table[i][z] << " ";
		cout << endl;
	}*/

	for (int k = 1; k < size; k++) { 
		if (table[k][j] > 0) {
			estimate[count] = table[k][0] / table[k][j];
			est_ind[count] = ind;
			count++;
		}

		ind++;
	}

	min = estimate[0];
	ind = 0;

	for (int k = 1; k < count; k++)
		if (estimate[k] < min) {
			min = estimate[k];
			ind = k;
		}

	i = est_ind[ind];

	delete []estimate;
	delete []est_ind;
}

bool SimplexMethod::noSolutionExists(double **table, const int &j, const int &rows) {
	for (int i = 1; i < rows; i++) { 
		if (table[i][j] > 0)
			return false;
	}

	return true;
}

bool SimplexMethod::allSurplusVarExcluded(char *row_literals, char *literals) {
	int i = 0, j = 0;
	bool found;

	while (row_literals[i] != '\0') {
		found = false;
		while (literals[j] != '\0') {
			if (row_literals[i] == literals[j]) {
				found = true;
				break;
			}
			j++;
		}

		if (!found)
			return false;

		i++;
	}

	return true;
}

char* SimplexMethod::define(vector<string> &equations, int &rows, int &columns) {
	rows = equations.size() + 1;
	columns = 0; 

	int ABC = 26;
	vector<string>::iterator it;
	map<char, bool> coefficients;
	char *literals = new char[ABC*2];
	int length, j = 0;

	char letter = 'A';
	for (int i = 0; i < ABC; i++) {
		coefficients[letter] = false;
		letter += 1;
	}

	letter = 'a';
	for (int i = 0; i < ABC; i++) {
		coefficients[letter] = false;
		letter += 1;
	}

	for (it = equations.begin(); it != equations.end(); ++it) {
		length = it->length();
			
		for (int i = 0; i < length; i++)
			if (Add::isCharacter((*it)[i]) && !coefficients[(*it)[i]]) {
				coefficients[(*it)[i]] = true;
				literals[j] = (*it)[i];
				columns += 1;
				j++;
			}
	}

	columns += 1 + equations.size(); //1 ������� ��� ���������� �����
	literals[j] = '\0';

	return literals;
}

//char* SimplexMethod::define(vector<string> &equations, int &rows, int &columns, char *row_literals) {
//	int ABC = 26;
//	char *literals = new char[ABC*2];
//	map<char, bool> coefficients;
//
//	char letter = 'A';
//	for (int i = 0; i < ABC; i++) {
//		coefficients[letter] = false;
//		letter += 1;
//	}
//
//	letter = 'a';
//	for (int i = 0; i < ABC; i++) {
//		coefficients[letter] = false;
//		letter += 1;
//	}
//
//	//for (int i = 0; i < rows; i++) 
//	//	if (Add::isCharacter(row_literals[i]))
//	//		coefficients[row_literals[i]] = true;
//
//	//count = 0;
//	//for (it = equations.begin(); it != equations.end(); ++it) {
//	//	length = it->length();
//
//	//	for (int i = 0; i < length; i++)
//	//		if (Add::isCharacter((*it)[i]) && !coefficients[(*it)[i]]) {
//	//			coefficients[(*it)[i]] = true;
//	//			literals[count] = (*it)[i];
//	//			columns += 1;
//	//			count++;
//	//		}
//	//}
//
//	//columns += rows;
//
//	return literals;
//
//
//}

char* SimplexMethod::defineSize(vector<string> &equations, int &rows, int &columns) {
	vector<string>::iterator it;
	int ABC = 26, length, lit_ind = 0, add_var_amount = 0;
	char *literals = new char[ABC*2];
	map<char, bool> coefficients;

	char letter = 'A';
	for (int i = 0; i < ABC; i++) {
		coefficients[letter] = false;
		letter += 1;
	}

	letter = 'a';
	for (int i = 0; i < ABC; i++) {
		coefficients[letter] = false;
		letter += 1;
	}

	for (it = equations.begin(); it != equations.end(); ++it) {
		length = it->length();
			
		for (int i = 0; i < length; i++)
			if (Add::isCharacter((*it)[i]) && !coefficients[(*it)[i]]) {
				coefficients[(*it)[i]] = true;
				literals[lit_ind] = (*it)[i];
				lit_ind++;
			}
	}

	literals[lit_ind] = '\0';

	for (it = equations.begin(); it != equations.end(); ++it) {
		length = it->length();

		for (int i = 0; i < length; i++) {
			if ((*it)[i] == '>') {
				add_var_amount++; break;
			}
		}
	}

	rows = equations.size() + 1;
	columns = rows + lit_ind + add_var_amount;

	return literals;
}

double** SimplexMethod::buildTable(vector<string> &equations, string key, char *column_literals, 
								   char *row_literals, char *literals, const int &rows, const int &columns) {
	int length, literal_ind = 0, equat_number = 1, max_length, i, j;
	char sign;
	vector<string>::iterator it;
	double **result;
	string temp;

	result = new double*[rows];
	for (i = 0; i < rows; i++)
		result[i] = new double[columns];

	for (i = 0; i < rows; i++)
		for (j = 0; j < columns; j++) 
			result[i][j] = 0;

	if (key == "natural") {	
		for (i = 1; i < rows; i++)
			result[i][i] = 1;

		for (i = 0; i < rows; i++) {
			row_literals[i] = ('0' + i);
			column_literals[i] = ('0' + i);
		}
		row_literals[rows] = '\0';

		for (i = rows; i < columns; i++) { //rows ������ ������ columns, ��� ��� ���� ����� ��������
			column_literals[i] = literals[i - rows];
			result[0][i] = -1; //������� z
		}	
	}

	if (key == "artificial") {
		bool *slack_var, *surplus_var;
		slack_var = new bool[rows];
		surplus_var = new bool[rows];
		string z_func = "";
		int k, new_var_amount = 0, col_coeff = 1;
		char letter;

		j = 0;
		for (it = equations.begin(); it != equations.end(); ++it) {
			i = 0;
			while (!Add::isRatioSign((*it)[i]))
				i++;

			if ((*it)[i] == '<') {
				slack_var[j] = true;
				surplus_var[j] = false;
				new_var_amount += 1;
			}
			if ((*it)[i] == '>') {
				slack_var[j] = true;
				surplus_var[j] = true;
				new_var_amount += 2;
			}
			if ((*it)[i] == '=') {
				slack_var[j] = false;
				surplus_var[j] = true;
				new_var_amount += 1;
			}
			j++;
		}

		column_literals[0] = '0';
		letter = 'a';
		//����������, ��� ������, ���� ���� �������� �� ������
		//� ������-�� ����� ������� ���������� ���� ����� �������� ��������� �����, ��� ����� ���������
		for (i = 0; i < new_var_amount; ) {
			if (letter < literals[0]) {
				column_literals[i + 1] = letter;
				letter++;
				i++;
			} else if (letter > literals[columns - new_var_amount]) {
				column_literals[i + 1] = letter;
				letter++;
				i++;
			} else
				letter++;
		}

		for (i = new_var_amount + 1; i < columns; i++)
			column_literals[i] = literals[i - (new_var_amount + 1)];

		row_literals[0] = '0';
		for (i = 1; i < rows; i++) {
			result[i][col_coeff] = 1;
			col_coeff++;
			row_literals[i] = column_literals[i];
			if (slack_var[i - 1] && surplus_var[i - 1]) {
				result[i][col_coeff] = -1;
				col_coeff++;
			}
		}
		row_literals[rows] = '\0';

		i = 0;
		for (it = equations.begin(); it != equations.end(); ++it) {
			if (surplus_var[i]) {
				length = (*it).length();
				
				for (k = 0; k < length; k++) {
					if (Add::isRatioSign((*it)[k])) {
						z_func += '-';
						k += 2; //���� ��������� � ������ ���� - ���� ��� �����
						if (Add::isActionSign((*it)[k]))
							k++;
					}
					z_func += (*it)[k];			
				}
			}
			i++;
		}
		for (i = 1; i < rows; i++) {
			if (slack_var[i - 1] && surplus_var[i - 1]) {
				j = 1;
				while (result[i][j] != -1) {
					j++;
					if (j > new_var_amount)
						break;
				}
				if (j <= new_var_amount) {
					z_func += '-';
					z_func += column_literals[j];
				}
			}
		}
		Add::canonize(z_func);

		length = z_func.length();
		max_length = z_func.length() - 1;
		for (i = 0; i < length; ) {
			if (Add::isActionSign(z_func[i])) {
				sign = z_func[i];
				i++;

				if (Add::isCharacter(z_func[i])) {
					literal_ind = literalInd(z_func[i], column_literals);
					if (sign == '-')
						result[0][literal_ind] = -1;
					else
						result[0][literal_ind] = 1;
				}	
				if (Add::isNumeric(z_func[i])) {
					char *number = new char[max_length];
					for (int k = 0; k < max_length; k++)
						number[k] = '.';
					int idx = 0;
					double coeff;
			
					while(Add::isNumeric(z_func[i]))
						if (idx < max_length) { 
							number[idx] = z_func[i];
							idx++; i++;
						}

					coeff = Add::charToDouble(number, idx);

					if (sign == '-')
						coeff = -coeff;

					delete []number;

					if (Add::isCharacter(z_func[i])) {
						literal_ind = literalInd(z_func[i], column_literals);
						result[0][literal_ind] = coeff;
					}

					if (Add::isActionSign(z_func[i]) || (z_func[i] == '\0')) {
						result[0][0] = -coeff;
					}
				}

				i++;
			} else
				i++;
		}

		delete[] slack_var;
		delete[] surplus_var;
	}

	column_literals[columns] = '\0';
			
	for (it = equations.begin(); it != equations.end(); ++it) {
		temp = *it;
		length = temp.length();
		max_length = length - 1;
		for (i = 0; i < length; ) {
			if (Add::isActionSign(temp[i])) {
				sign = temp[i];
				i++;

				if (Add::isCharacter(temp[i])) {
					literal_ind = literalInd(temp[i], column_literals);
					if (sign == '-')
						result[equat_number][literal_ind] = -1;
					else
						result[equat_number][literal_ind] = 1;
				}	
				if (Add::isNumeric(temp[i])) {
					char *number = new char[max_length];
					for (int k = 0; k < max_length; k++)
						number[k] = '.';
					int idx = 0;
					double coeff;
			
					while(Add::isNumeric(temp[i]))
						if (idx < max_length) { 
							number[idx] = temp[i];
							idx++; i++;
						}

					coeff = Add::charToDouble(number, idx);

					if (sign == '-')
						coeff = -coeff;

					delete []number;

					if (Add::isCharacter(temp[i])) {
						literal_ind = literalInd(temp[i], column_literals);
						result[equat_number][literal_ind] = coeff;
					}

					if (Add::isActionSign(temp[i]) || (Add::isRatioSign(temp[i])) || (temp[i] == '\0')) {
						result[equat_number][0] = coeff;
					}
				}

				i++;
			} else
				i++;
		}
		equat_number += 1;
	}

	delete []literals;

	return result;
}

double** SimplexMethod::rebuildTable(double **table, int a, int b, char *column_literals, char *row_literals) {
	int rows = 0, columns = 0;
	while (column_literals[columns] != '\0')
		columns++;
	while (row_literals[rows] != '\0')
		rows++;

	double **new_table = new double*[rows];
	for (int i = 0; i < rows; i++)
		new_table[i] = new double[columns];

	row_literals[a] = column_literals[b];

	for (int i = 0; i < columns; i++)
		new_table[a][i] = table[a][i] / table[a][b];

	for (int i = 0; i < rows; i++)
		for (int j = 0; j < columns; j++)
			if (i != a)
				new_table[i][j] = table[i][j] - new_table[a][j]*table[i][b];

	return new_table;
}

//double* SimplexMethod::makeEstimation(double **table, char *column_literals, char *row_literals) {
//	//if this isn't possible, NULL returns
//	int columns = column_literals.length(), rows = row_literals.length();
//	double sum, *result;
//	bool was_possible = false;
//	
//	result = new double[columns - 1];
//
//	for (int i = 1; i < columns; i++) {
//		sum = 0;
//		for (int j = 1; j < rows; j++) {
//			if (Add::isNumeric(row_literals[j])) {
//				sum += table[j][i];
//			}
//		}
//
//		result[i - 1] = -sum;
//	}
//
//	columns -= 1;
//	for (int i = 0; i < columns; i++) {
//		if (result[i] != 0) {
//			was_possible = true;
//			break;
//		}
//	}
//
//	if (!was_possible) {
//		delete[] result;
//		result = NULL;
//	}
//
//	return result;
//}

bool SimplexMethod::isPositive(double *estimation, int size) {
	for (int i = 0; i < size; i++)
		if (estimation[i] < 0)
			return false;

	return true;
}

//bool SimplexMethod::artificialVariablesExist(char *row_literals) {
//	int size = row_literals.length();
//
//	for (int i = 1; i < size; i++)
//		if (Add::isNumeric(row_literals[i]))
//			return true;
//
//	return false;
//}

void SimplexMethod::pullUpSolutions(double **table, char *row_literals, char *column_literals) {
	int rows = 0, count = 0, columns = 0;
	while (row_literals[rows] != '\0')
		rows++;
	while (column_literals[columns] != '\0')
		columns++;

	map<char, double> answers;
	map<char, double>::iterator it;

	cout << "SATISFIABLE" << endl;

	while (!Add::isCharacter(column_literals[count++]));

	for (int i = count - 1; i < columns; i++)
		answers[column_literals[i]] = 0;

	for (int i = 0; i < rows; i++) {
		if (Add::isCharacter(row_literals[i])) 	
			answers[row_literals[i]] = table[i][0];
	}

	for (it = answers.begin(); it != answers.end(); ++it) {
		cout << (*it).first << " = " << (*it).second << "\t" << endl;
	}

}

bool SimplexMethod::useArtificialBasis(vector<string> &equations) {
	char *column_literals = NULL, *row_literals = NULL, *literals = NULL;
	int rows = 0, columns = 0, i, j;
	double **table;

	literals = defineSize(equations, rows, columns);
	column_literals = new char[columns];
	row_literals = new char[rows];

	table = buildTable(equations, "artificial", column_literals, row_literals, literals, rows, columns);

	cout << endl << endl << "wwwwwwwwwwww" << endl;
	for (i = 0; i < rows; i++) {
		for (j = 0; j < columns; j++)
			cout << table[i][j] << " ";
		cout << endl;
	}

	while ((table[0][0] != 0) && (!allSurplusVarExcluded(row_literals, literals))) {
		if ((table[0][0] != 0) && (allSurplusVarExcluded(row_literals, literals)))
			return false;

		if (table[0][0] == 0) {
			//� ���� ��� safe row, � table[0][0] = 0?
			//����� ������������ "������������ ��������������".
			//������, ���� ����� ��� �����������
			/*chooseSafeRow(table, i, j, row_literals, column_literals);
			multiply();*/
			table = rebuildTable(table, i, j, column_literals, row_literals);

			/*cout << endl << endl << "wwwwwwwwwwww" << endl;
			for (i = 0; i < rows; i++) {
				for (j = 0; j < columns; j++)
					cout << table[i][j] << " ";
				cout << endl;
			}*/
		}

		if (table[0][0] != 0) {
			choosePivotColumn(table, j, column_literals);
			///?
			if (noSolutionExists(table, j, rows))
				return false;
			choosePivotRow(table, i, j, row_literals);
			table = rebuildTable(table, i, j, column_literals, row_literals);

			/*cout << endl << endl << "wwwwwwwwwwww" << endl;
			for (i = 0; i < rows; i++) {
				for (j = 0; j < columns; j++)
					cout << table[i][j] << " ";
				cout << endl;
			}*/
		}
	}

	return true;
}

void SimplexMethod::useNatualBasisWithSolutions(vector<string> &equations) {
	char *row_literals, *column_literals, *literals;
	double **table;
	int i, j, rows, columns;

	literals = define(equations, rows, columns);
	column_literals = new char[columns + 1];
	row_literals = new char[rows + 1];

	table = buildTable(equations, "natural", column_literals, row_literals, literals, rows, columns);

	while (isElementBelowZero(table, column_literals)) {
		choosePivotColumn(table, j, column_literals);
		choosePivotRow(table, i, j, row_literals);
		table = rebuildTable(table, i, j, column_literals, row_literals);
	}

	pullUpSolutions(table, row_literals, column_literals);

	delete[] column_literals;
	delete[] row_literals;
}

void SimplexMethod::useArtificialBasisWithSolutions(vector<string> &equations) {
	/*string column_literals, row_literals, prev_row_literals;
	double **table, *estimation;
	int i, j, size;

	table = buildTable(equations, "artificial", column_literals, row_literals);

	while (artificialVariablesExist(row_literals)) {
		estimation = makeEstimation(table, column_literals, row_literals);
		size = column_literals.length() - 1;

		if (estimation == NULL)
			choosePivotColumn(table, j, column_literals);
		else
			choosePivotColumn(j, estimation, size);

		choosePivotRow(table, i, j, row_literals);
		prev_row_literals = row_literals;
		table = rebuildTable(table, i, j, column_literals, row_literals);

		if (row_literals == prev_row_literals)
			break;
	}

	pullUpSolutions(table, row_literals, column_literals);*/
}