#include "additional_functions.h"

bool Add::isNumeric(char ch) {
	if (((ch >= '0') && (ch <= '9')) ||	(ch == '.'))
		return true;
	return false;
}

bool Add::isActionSign(char ch) {
	if ((ch == '+') || (ch == '-') ||
		(ch == '*') || (ch == '|'))
		return true;
	return false;
}

bool Add::isRatioSign(char ch) {
	if ((ch == '<') || (ch == '>') ||
		(ch == '=') )
		return true;
	return false;
}

bool Add::isCharacter(char ch) {
	if (((ch >= 'A') && (ch <= 'Z')) ||
		((ch >= 'a') && (ch <= 'z')))
		return true;
	return false;
}

bool Add::acceptable(char ch) {
	if (isNumeric(ch) || isCharacter(ch) ||
		isActionSign(ch) || isRatioSign(ch) || (ch == '.'))
		return true;
	return false;
}

void Add::keyError() {
	cerr << "Error 3: Key words error."
		"\nPlease define them as it shown in 'ReadMe.txt'\n";
	exit(3);
}

void Add::clauseNumberError() {
	cerr << "Error 4: Unacceptable number of clauses\n";
	exit(4);
}

void Add::partitionNumberError() {
	cerr << "Error 5: Unacceptable number of partition\n";
	exit(5);
}

void Add::sizeError() {
	cerr << "Error 6: Sizes were not defined before data."
		"\nPlease define sizes as it shown in 'ReadMe.txt'\n";
	exit(6);
}

void Add::linesError() {
	cerr << "Error 7: Actual number of clauses is different from declared number\n";
	exit(7);
}

void Add::inputError() {
	cerr << "Error 8: Input error. Please check input file for garbage\n";
	exit(8);
}

void Add::formatError() {
	cerr << "Error 9: File format error. \n"
		"Please check input file for unacceptable symbols\n";
	exit(9);
}

double Add::charToDouble(char* number, int size) {
	double result = 0;
	int i = 0, k = 0, degree = 0;

	while (number[i] != '.') {
		degree++; i++;
	}

	for (int i = 0; i < size; i++) {
		if (number[i] != '.') {
			result += (number[i] - '0')*pow(10.0, degree - (k + 1));
			k++;
		}			
	}

	return result;
}

string Add::doubleToString(double number) {
	stringstream temp;
	string result;

	if (number > 0)
		temp << "+" << number;
	else
		temp << number;

	result = temp.str();
	return result;
}

double Add::setCoeff(double value, char &sign, bool after_ratio_sign) {
	if (after_ratio_sign) {
		if (sign == '+')
			sign = '-';
		else
			sign = '+';
	} 
	if (sign == '-')
		return -value; 
	if (sign == '+')
		return value;

	sign = ' ';
	return value;
}

bool Add::nonLiteralEquation(const string &data) {
	int size = data.length();

	for (int i = 0; i < size; i++)
		if (Add::isCharacter(data[i]))
			return false;

	return true;
}

void Add::canonize(string &equation) {
	map<char, double> coefficients;
	map<char, double>::iterator it;
	int length, MAX_LENGTH, ABC = 26;
	char sign;
	string ratio_sign = "", temp = "";
	bool after_ratio_sign = false, need_to_reverse_signs = false;
	
	length = equation.length();
	char letter = 'A';
	for (int i = 0; i < ABC; i++) {
		coefficients[letter] = 0;
		letter += 1;
	}

	letter = 'a';
	for (int i = 0; i < ABC; i++) {
		coefficients[letter] = 0;
		letter += 1;
	}

	coefficients[' '] = 0;

	MAX_LENGTH = equation.length() - 1; //the maximum length of the coefficient 

	for (int i = 0; i < length; i++) {
		equation[i];
		if (Add::isActionSign(equation[i])) {
			sign = equation[i];
			i++;

			if (Add::isCharacter(equation[i]))
				coefficients[equation[i]] += setCoeff(1, sign, after_ratio_sign);
			if (Add::isNumeric(equation[i])) {

				char *number = new char[MAX_LENGTH];
				for (int k = 0; k < MAX_LENGTH; k++) {
					number[k] = '.';
				}
				int idx = 0;
				double coeff;
			
				while(Add::isNumeric(equation[i]))
					if (idx < MAX_LENGTH) { 
						number[idx] = equation[i];
						idx++; i++;
					}

				coeff = Add::charToDouble(number, idx);

				delete []number;

				if (Add::isCharacter(equation[i])) {
					coefficients[equation[i]] += setCoeff(coeff, sign, after_ratio_sign);
				}

				if ((equation[i] == ' ') || (equation[i] == '\0')) {
					coefficients[' '] += setCoeff(coeff, sign, after_ratio_sign);
				}

				if (Add::isRatioSign(equation[i]) || Add::isActionSign(equation[i])) {
					i--;
					coefficients[' '] += setCoeff(coeff, sign, after_ratio_sign);
				}
			}

		} else if (Add::isNumeric(equation[i])) {
			sign = '+';

			char *number = new char[MAX_LENGTH];
			for (int k = 0; k < MAX_LENGTH; k++) {
				number[k] = '.';
			}

			int idx = 0;
			double coeff;
			
			while(Add::isNumeric(equation[i]))
				if (idx < MAX_LENGTH) { 
					number[idx] = equation[i];
					idx++; i++;
				}

			coeff = Add::charToDouble(number, idx);

			delete []number;

			if (Add::isCharacter(equation[i])) {
				coefficients[equation[i]] += setCoeff(coeff, sign, after_ratio_sign);
			}

			if ((equation[i] == ' ') || (equation[i] == '\0')) {
				coefficients[' '] += setCoeff(coeff, sign, after_ratio_sign);
			}

			if (Add::isActionSign(equation[i]) || Add::isRatioSign(equation[i])) {
				i--;
				coefficients[' '] += setCoeff(coeff, sign, after_ratio_sign);
			}
		} else if (Add::isRatioSign(equation[i])) {
			ratio_sign += equation[i];
			after_ratio_sign = true;
			i++;

			if (Add::isRatioSign(equation[i]))
				ratio_sign += equation[i];
			else {
				ratio_sign += ' ';
				i--;
			}

		} else if (Add::isCharacter(equation[i])) {
			sign = '+';
			coefficients[equation[i]] += setCoeff(1, sign, after_ratio_sign);
		}
	}

	if (!nonLiteralEquation(equation)) {
		if ((coefficients[' '] > 0) && after_ratio_sign) {
			need_to_reverse_signs = true;
			coefficients[' '] = -coefficients[' '];
		}

		it = coefficients.begin();
		it++; //����� ������ ������� coefficients[' ']

		for (it; it != coefficients.end(); ++it) {
			if ((*it).second != 0) {
				if (need_to_reverse_signs) 
					temp += Add::doubleToString(-(*it).second);
				else
					temp += Add::doubleToString((*it).second);

				temp += (*it).first;
			}
		}
	}

	if (need_to_reverse_signs) {
		if (ratio_sign[0] == '>')
			ratio_sign[0] = '<';
		else 
			ratio_sign[0] = '>';
	}

	temp += ratio_sign;

	if (after_ratio_sign)
		temp += Add::doubleToString(-coefficients[' ']);
	else
		temp += Add::doubleToString(coefficients[' ']);

	equation = temp;
} 