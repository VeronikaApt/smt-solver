#include "predicate.h"

Predicate::Predicate() {
	data = "";
}

void Predicate::setData(char ch) {
	data += ch;
}

char Predicate::getData(int i) {
	if (data.length() > i)
		return data[i];
	return ' ';
}

Predicate& Predicate::operator=(Predicate _predicate) {
	data = _predicate.data;
	return *this;
}

int Predicate::getLength() {
	return data.length();
}

string Predicate::getData() {
	return data;
}