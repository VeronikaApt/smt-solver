#pragma once
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <map>

using namespace std;

class Add {
public:
	static bool isNumeric(char ch);
	static bool isActionSign(char ch);
	static bool isRatioSign(char ch);
	static bool isCharacter(char ch);
	static bool acceptable(char ch);
	static void keyError();
	static void clauseNumberError();
	static void partitionNumberError();
	static void sizeError();
	static void linesError();
	static void inputError();
	static void formatError();
	static double charToDouble(char* number, int size);
	static string doubleToString(double number);
	static double setCoeff(double value, char &sign, bool after_ratio_sign);
	static bool nonLiteralEquation(const string &data);
	static void canonize(string &equation);	
};