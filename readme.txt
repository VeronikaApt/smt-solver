There is an attempt to create a simple SMT-solver with only linear inequalities theory.
In the mean time you can only work with the inequalities "<=" class. 
In the input file first number is the number of clauses and second number is the max number of predicate in whole inequalities system.
Example of input file:
here we go 3 2
2x+3y-4z<=15 /
3x+5y<=10 | 2y+19z<=5 /
6x<=8 /
%