#pragma once
#include "predicate.h"
#include "simplex_method.h"

class LinearInequalities {
	vector<string> limitations;
public:
	bool assignTruth(Predicate &predicate);
	void popBack();
	void solve();
};