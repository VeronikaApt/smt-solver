#include "linear_inequalities.h"

bool LinearInequalities::assignTruth(Predicate &predicate) {
	string data = predicate.getData();
	string basis;
	bool is_done;

	Add::canonize(data);
	limitations.push_back(data);

	if ((limitations.size() == 1) && (!Add::nonLiteralEquation(data))) 
		return true;
	
	basis = SimplexMethod::defineBasis(limitations);

	if (basis == "natural") {
		return true;
	}
	if (basis == "artificial") {
		if (Add::nonLiteralEquation(data)) {
			if (data.length() == 1) //������� ������. ����������� ���� "="
				return true;

			if ((data.length() == 2) && (data[1] == '=')) //������� ������. ����������� ���� ">="
				return true;

			if ((data[1] == '+') || (data[2] == '+'))
				return true;
			else
				return false;

		} else {
			is_done = SimplexMethod::useArtificialBasis(limitations);
			if (!is_done) {
				limitations.pop_back();
				return false;
			}	
		}
	}

	return true;
}

void LinearInequalities::popBack() {
	limitations.pop_back();
}

void LinearInequalities::solve() {
	string basis;
	basis = SimplexMethod::defineBasis(limitations);

	if (basis == "natural") {
		SimplexMethod::useNatualBasisWithSolutions(limitations);			
	} 
	if (basis == "artificial") {
		SimplexMethod::useArtificialBasisWithSolutions(limitations);		
	}
}