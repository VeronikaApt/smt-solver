#pragma once
#include <string>

using namespace std;

class Predicate {
private:
	string data;
public:
	Predicate();
	void setData(char ch);
	char getData(int i);
	string getData();
	int getLength();

	Predicate &operator=(Predicate _predicate);
};

