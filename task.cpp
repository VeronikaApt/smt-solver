#include "task.h"

Task::Task() {
	number_of_clauses = 0;
}

Task::~Task() {
	for (int i = 0; i < number_of_clauses; i++)
		delete []data[i];
	delete []data;

	delete []partition_count;
}

Predicate Task::takePiece(bool &there_is, int piece_count, int clause_ind) {
	Predicate result;
	if (piece_count >= partition_count[clause_ind])
		there_is = false;
	else {
		there_is = true;
		result = data[clause_ind][piece_count];
	}

	return result;
}

string Task::chooseTheory(Predicate &predicate) {
	int length = predicate.getLength();
	string data = predicate.getData();
	bool linear_inequalities;
	string result;

	linear_inequalities = false;
	for (int i = 0; i < length; i++) {
		if ((data[i] == '>') || (data[i] == '<') || (data[i] == '=')) {
			linear_inequalities = true;
			break;
		}
		//��� ���������� ������ ������ ���� ������� ���� �������������
		/*if (data[i] == '^')
			linear_inequalities = false;*/
	}

	if (linear_inequalities)
		result = "Linear inequalities";

	return result;
}

bool Task::allClausesAssigned(bool *assigned_clauses) {
	for (int i = 0; i < number_of_clauses; i++)
		if (assigned_clauses[i] == false)
			return false;

	return true;
}

void Task::readData(ifstream &input) {
	char ch;
	bool size_is_defined = false;
	int line = -1;
	int SIZE_OF_PREDICATE = 50;
	char key[] = "here we go ";
	int size_of_key = sizeof(key) / sizeof(key[0]);
	
	do {
		ch = input.peek();

		//Processing string containing the number of clauses
		if (ch == key[0]) {
			for (int i = 0; i < size_of_key - 1; i++) {
				ch = input.get();
				if (ch != key[i])
					Add::keyError();
			}

			input >> number_of_clauses;
			if (input.fail() || (number_of_clauses <= 0))
				Add::clauseNumberError();
			
			input >> number_of_partition;
			if (input.fail() || (number_of_partition <= 0))
				Add::partitionNumberError();

			data = new Predicate*[number_of_clauses];
			
			for (int i = 0; i < number_of_clauses; i++)
				data[i] = new Predicate[number_of_partition];

			partition_count = new int[number_of_clauses];

			size_is_defined = true;

			input.get();

			continue;
		}
			
		//Processing string with the numbers
		if (Add::acceptable(ch)) {
			if (!size_is_defined)
				Add::sizeError();

			line += 1;
			if (line >= number_of_clauses)
				Add::linesError();
			int partition = 0;
			char character = input.peek();

			while (character != '/') {
				int column = 0;
				bool end_line = false; 
				
				Predicate predicate;

				do { 
					input >> character;
					if (!input)
						Add::inputError();

					if ((character == '|') || (character == '/')) {
						end_line = true;
					} else {
						predicate.setData(character);
						column++;
					}
				} while (!end_line);
 
				data[line][partition] = predicate;
				partition++;
			}

			partition_count[line] = partition;
		}

		if ((ch == '\n') || (ch == ' ') || (ch == '/'))
			input.get();

		if ((ch == '%') && (line < number_of_clauses - 1))
			Add::linesError();

		if (!Add::acceptable(ch) && (ch != '\n') && (ch != ' ') && (ch != '/') && (ch != '%'))
			Add::formatError();

	} while (ch != '%');
}

void Task::solve() {
	bool done = false, there_is = true, exit = false, there_is_a_way = true;
	bool found = false, assigned = false, unsatisfiable = false;
	bool *assigned_clauses = new bool[number_of_clauses];
	int piece_count = 0, clause_ind = 0;
	vector<string> used_theories;
	vector<string>::iterator it;
	LinearInequalities lin_ineq;
	Predicate piece;
	string theory;

	for (int i = 0; i < number_of_clauses; i++)
		assigned_clauses[i] = false;

	bool **define_twice = new bool*[number_of_clauses];
	for (int i = 0; i < number_of_clauses; i++) {
		define_twice[i] = new bool[partition_count[i]];
	}

	for (int i = 0; i < number_of_clauses; i++)
		for (int j = 0; j < partition_count[i]; j++)
			define_twice[i][j] = false;

	while (!exit) {
		done = false;
		while (!done) {
			piece = takePiece(there_is, piece_count, clause_ind);
			if (there_is) {
				theory = chooseTheory(piece);

				for (it = used_theories.begin(); it != used_theories.end(); ++it) {
					if (*it == theory) {
						assigned = true;
						break;
					}
				}

				if (!assigned)
					used_theories.push_back(theory);

				if (theory ==  "Linear inequalities") {
					done = lin_ineq.assignTruth(piece);
				}

				if (done)
					assigned_clauses[clause_ind] = true;
				else {
					piece_count++;

					if (piece_count >= partition_count[clause_ind]) {
						cout << "UNSATISFIABLE" << endl; 
						unsatisfiable = true;
						done = true;
						exit = true;
					}					
				}
			} else {
				done = false;
				while (!done) {
					found = false;
					while (!found) {
						if (clause_ind == 0) {
							there_is_a_way = false;
							break;
						}

						clause_ind--;
						piece_count = 0;

						while (define_twice[clause_ind][piece_count]) {
							piece_count++;
							if (piece_count == partition_count[clause_ind])
								break;
						}

						if (piece_count != partition_count[clause_ind])
							found = true;
					}

					piece = takePiece(there_is, piece_count, clause_ind);
					theory = chooseTheory(piece);

					if (theory ==  "Linear inequalities") {
						lin_ineq.popBack();
					}

					define_twice[clause_ind][piece_count] = true;

					if (piece_count == partition_count[clause_ind] - 1) {
						clause_ind--;
						piece_count = 0;
					} else {
						piece_count++;
						done = true;
					}
				}
			}
		}
		
		if (!exit) {
			if (allClausesAssigned(assigned_clauses))
				exit = true;
			else {
				if (there_is_a_way) {
					clause_ind++;
					piece_count = 0;
				} else {
					cout << "UNSATISFIABLE" << endl; 
					unsatisfiable = true;
					exit = true;
				}
			}
		}
	}

	if (!unsatisfiable) {
		for (it = used_theories.begin(); it != used_theories.end(); ++it) {
			if (*it == "Linear inequalities")
				lin_ineq.solve();
		}
	}

	delete []assigned_clauses;
}

