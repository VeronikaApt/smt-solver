#pragma once
#include "linear_inequalities.h"

class Task {
private:
	Predicate **data;
	int number_of_clauses;
	int number_of_partition; //максимальное число предикатов
	int *partition_count;

	Predicate takePiece(bool &there_is, int piece_count, int clause_ind);
	string chooseTheory(Predicate &predicate);
	bool assignTruth(Predicate &piece, LinearInequalities &lin_ineq);
	bool allClausesAssigned(bool *assigned_clauses);
public:
	Task();
	~Task();

	void readData(ifstream &input);
	void solve();
};