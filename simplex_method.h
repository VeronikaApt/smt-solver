#pragma once
#include <vector>
#include "additional_functions.h"

using namespace std;

class SimplexMethod {
private:
	static int literalInd(char ch, char *column_literals);
	static bool isElementBelowZero(double **table, char *column_literals);
	static bool allElementBelowZero(double **table, char *row_literals, int j);
	static void choosePivotColumn(double **table, int &j, char *column_literals);
	//static void choosePivotColumn(int &j, double *estimation, int size);
	static void choosePivotRow(double **table, int &i, int j, char *row_literals);
	static bool noSolutionExists(double **table, const int &j, const int &rows);
	static bool allSurplusVarExcluded(char *row_literals, char *literals);
	static char* define(vector<string> &equations, int &rows, int &columns);
//	static char* define(vector<string> &equations, int &rows, int &columns, char *row_literals);
	static char* defineSize(vector<string> &equations, int &rows, int &columns);
	static double** buildTable(vector<string> &equations, string key, char *column_literals, 
		char *row_literals, char *literals, const int &rows, const int &columns);
	static double** rebuildTable(double **table, int a, int b, char *column_literals, char *row_literals);
	//static double* makeEstimation(double **table, char *column_literals, char *row_literals);
	static bool isPositive(double *estimation, int size);
	//static bool artificialVariablesExist(char *row_literals);
	static void pullUpSolutions(double **table, char *row_literals, char *column_literals);
public:
	static string defineBasis(vector<string> &equations);
	static bool useArtificialBasis(vector<string> &equations);
	static void useNatualBasisWithSolutions(vector<string> &equations);
	static void useArtificialBasisWithSolutions(vector<string> &equations);
};